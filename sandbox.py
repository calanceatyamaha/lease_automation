import requests, time, re
from xlsxwriter.workbook import Workbook

# def make_workbook():
    # ismonth = time.strftime('%b%Y')
    # workbook = Workbook('MaturingLeasesAsOf' + ismonth + '.xlsx')

def get_endpoint():
    '''This is the endpoint for all API calls.
    '''
    amos = 'https://amosapi.ifsleasing.com/'
    return amos

def get_auth():
    '''API call to get the autorization key which will need to be rentued with each run.
    '''
    user_name = 'cctech'
    passw = 'cH@rger16!'
    r = requests.get(get_endpoint() + 'authenticate?username=' + user_name +'&password=' + passw)
    amos_key = r.json()["Data"]
    return amos_key

def get_account():
    '''Returns the account.
    '''
    account = requests.get(get_endpoint() + 'accounts?key=' + get_auth())
    return account.json()["Data"][0]["Id"]

def get_contracts():
    '''Returns Active contracts as a json
    '''
    contracts = requests.get(get_endpoint() + 'contracts/?xPageAccountId=' + str(get_account()) + "&key=" + get_auth())
    return contracts.json()["Data"]

def get_all_leases():
    '''Returns all leases as a list of dicts.
    '''
    leases = dict()
    lease_list = dict()
    for l in get_contracts():
        leases.update({'Status':l["MaturityStatus"]})
        leases.update({'Lease Number':l["LeaseNumber"]})
        leases.update({'End Date': time.strftime('%m/%d/%Y', time.gmtime(float(re.findall('\\b\\d+\\b', l["ProjectedEndDate"])[0])/1000))})
        lease_list.update(leases)
        # return leases
    return lease_list


def get_mature_leases():
    '''Returns leases that are x months from maturity.
    '''
    months = str(4)
    upcomingMaturities = requests.get(get_endpoint() + 'contracts/upcomingmaturities?xPageAccountId=10&key=' + get_auth() + '&maturityRange=' + months + '&queryType=3')
    m_leases = list()
    for i in upcomingMaturities.json()["Data"]:
        m_leases.append(i["Id"])
    return m_leases

def get_mature_assets():
    '''Returns a *something* with all of the assets that will be maturing
    '''
    line = list()
    # https://amosapi.ifsleasing.com/contractassets/CONTRACTID?xPageAccountId=ACCOUNTID&key=EXAMPLEKEY&p=PAGENUMBER&udf=INCLUDEUDF
    for lease in get_mature_leases():
        thing = requests.get(get_endpoint() + 'contractassets/' + str(lease) + '?xPageAccountId=10&key=' + get_auth())
        for i in thing.json()["Data"]["AssetDetails"]:
            line.append(i["Id"])
            line.append(i["SerialNumber"])
            line.append(i["Description"])
            line.append(i["Reference"])
        return line
    return lease

def get_assets(leaseid):
    '''Returns assets for given lease.
    '''
    line = list()
    thing = requests.get(get_endpoint() + 'contractassets/' + str(leaseid) + '?xPageAccountId=10&key=' + get_auth())
    for i in thing.json()["Data"]["AssetDetails"]:
        line.append(i["Id"])
        line.append(i["SerialNumber"])
        line.append(i["Description"])
        line.append(i["Reference"])
    return line

def get_lease_from_id(id):
    '''Returns the lease number based off of the contract ID
    '''
    url = requests.get(get_endpoint() + 'contractassets/' + str(id) + '?xPageAccountId=10&key=' + get_auth())
    con = list()
    return url.json()["Data"]["LeaseNumber"]


def write_workbook():
    '''Writes the .xlsx based off of the maturing leases.
    '''
    ismonth = time.strftime('%b%Y')
    workbook = Workbook('MaturingLeasesAsOf' + ismonth + '.xlsx')
    for i in get_mature_leases():
        worksheet = workbook.add_worksheet('Lease ' + get_lease_from_id(i))
        row = 0
        col = 0
        for l in get_assets(i):
            worksheet.write(row, col, l)
            col += 1
            if col == 4:
                row += 1
                col = 0

    # for l in get_mature_assets():
    #     worksheet.write(row, col, l)
    #     col += 1
    #     if col == 4:
    #         row += 1
    #         col = 0

    workbook.close()



if __name__ =='__main__':
    write_workbook()
