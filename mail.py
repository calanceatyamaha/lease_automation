#!/usr/bin/env python
import smtplib, datetime
from email.mime.text import MIMEText

cctech = "ybp116.cctech@gmail.com"
recipiant = "cwelker@calance.com"
ts = str(datetime.datetime.now()).split('.')[0]


def mac_remote_lease_swap():


    html = """\
    <html>
    <head></head>
    <body>
    <p>Hi <var>fistName</var> <var>lastName</var>,</p>

    <p>Your new Mac (to replace <strong>TAGA<var>tagNumber</var></strong>) has arrived and we are beginning to configure it for deployment.<br>
    Could you please let us know the following information?</p>

    <ol type="1">
      <li>What is your network password? (You can always change it afterward, or call us directly to provide the password)</li>

      <li>What programs do you need us to ensure are installed and running properly?<br>
      <strong>(MS Office 2016, Pages, Numbers and Keynotes are included with the new Mac)</strong></li>

      <li>Do you require Windows?</li>

      <li>When would be a good time to schedule the lease swap?  Keep in mind that with shipping and a 1-2 day turnaround time you will be without a computer for 3-5 days.</li>
    </ol>

    <p>Below is the equipment check list of everything that need to be returned to the leasing company.  If any of the following equipment are not account for,
    see that attached email for instruction on how to file a Loss Asset Form to HR.  Contact your lease admin if you need further assistance with the process.</p>

    <ul style="list-style-type:disc">
      <li><strong><var>returnItems[0]</var></strong></li>
    </ul>

    <p>Please provide your mailing address and phone number. Return the old equipment via priority shipment to the following address. Always remember to provide the tracking information since it is the only way we can confirm the return of the equipment and also file a claim if the equipment is damaged during shipping.</p>

    <p>Thank you,<br>
      Your Yamaha Desktop Support Team</p>

    <p>CCTech<br>
    6600 Orangethorpe Avenue<br>
    Buena Park, CA 90620<br>
    714-522-9925</p>
    </body>
    </html>
    """
    msg = MIMEText(html, 'html')

    cr = "xxxxx"
    msg['Subject'] = 'CR:' + cr + " - Computer Swap"
    msg['From'] = cctech
    msg['To'] = recipiant

    part1 = MIMEText(html, 'html')

    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.login('ybp116.cctech','cH@rger16!')
    s.sendmail(cctech, recipiant, msg.as_string())
    s.quit()

def mac_office_lease_swap():


    html = """\
    <html>
    <head></head>
    <body>
    <p>Hi <var>fistName</var> <var>lastName</var>,</p>

    <p>Your new Mac (to replace <strong>TAGA<var>tagNumber</var></strong>) has arrived and we are beginning to configure it for deployment.<br>
    Could you please let us know the following information?</p>

    <ol type="1">
      <li>What is your network password? (You can always change it afterward, or call us directly to provide the password)</li>

      <li>What programs do you need us to ensure are installed and running properly?<br>
      <strong>(MS Office 2016, Pages, Numbers and Keynotes are included with the new Mac)</strong></li>

      <li>Do you require Windows? If yes, please be aware that your lease swap will take twice as long as normal since we will have to install two operation systems on the same computer.</li>

      <li>Use the attached document to schedule an appointment from the date of 8/9 to 9/1/2017 for your lease swap on SharePoint. If your target date is taken, contact us directly to see if we can accommodate your request. Keep in mind that we would need your current desktop for at least two hours (or more if you require Windows) to transfer your personal data and settings over to your new Mac.</li>
    </ol>

    <p>Below is the equipment check list of everything that need to be returned to the leasing company.  If any of the following equipment are not account for,
    see that attached email for instruction on how to file a Loss Asset Form to HR.  Contact your lease admin if you need further assistance with the process.</p>

    <ul style="list-style-type:disc">
      <li><strong><var>returnItems[0]</var></strong></li>
    </ul>

    <p>Thank you,<br>
      Your Yamaha Desktop Support Team</p>

    </body>
    </html>
    """
    msg = MIMEText(html, 'html')

    cr = "xxxxx"
    msg['Subject'] = 'CR:' + cr + " - Computer Swap"
    msg['From'] = cctech
    msg['To'] = recipiant

    part1 = MIMEText(html, 'html')

    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.login('ybp116.cctech','cH@rger16!')
    s.sendmail(cctech, recipiant, msg.as_string())
    s.quit()

def laptop_office_lease_swap():


    html = """\
    <html>
    <head></head>
    <body>
    <p>Hi <var>fistName</var> <var>lastName</var>,</p>

    <p>Your new Dell Laptop (to replace <strong>TAGA<var>tagNumber</var></strong>) has arrived and we are beginning to configure it for deployment.<br>
    Could you please let us know the following information?</p>

    <ol type="1">
      <li>What is your Wave encryption password? (You can always change it afterward, or call us directly to provide the password)</li>

      <li>What is your network password, if it is different than Wave?</li>

      <li>What programs do you need us to ensure are installed and running properly?</li>

      <li>Use the attached document to schedule an appointment from the date of 8/9 to 9/1/2017 for your lease swap on SharePoint. If your target date is taken, contact us directly to see if we can accommodate your request. Keep in mind that we would need your current desktop for at least two hours (or more if you require Windows) to transfer your personal data and settings over to your new Mac.</li>
    </ol>

    <p>Below is the equipment check list of everything that need to be returned to the leasing company.  If any of the following equipment are not account for,
    see that attached email for instruction on how to file a Loss Asset Form to HR.  Contact your lease admin if you need further assistance with the process.</p>

    <ul style="list-style-type:disc">
      <li><strong><var>returnItems[0]</var></strong></li>
    </ul>

    <p>Thank you,<br>
      Your Yamaha Desktop Support Team</p>

    </body>
    </html>
    """
    msg = MIMEText(html, 'html')

    cr = "xxxxx"
    msg['Subject'] = 'CR:' + cr + " - Computer Swap"
    msg['From'] = cctech
    msg['To'] = recipiant

    part1 = MIMEText(html, 'html')

    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.login('ybp116.cctech','cH@rger16!')
    s.sendmail(cctech, recipiant, msg.as_string())
    s.quit()

def laptop_remote_lease_swap():


    html = """\
    <html>
    <head></head>
    <body>
    <p>Hi <var>fistName</var> <var>lastName</var>,</p>

    <p>Your new Dell Laptop (to replace <strong>TAGA<var>tagNumber</var></strong>) has arrived and we are beginning to configure it for deployment.<br>
    Could you please let us know the following information?</p>

    <ol type="1">
      <li>What is your Wave encryption password? (You can always change it afterward, or call us directly to provide the password)</li>

      <li>What is your network password, if it is different than Wave?</li>

      <li>What programs do you need us to ensure are installed and running properly?</li>

      <li>4)	When would be a good time to schedule the lease swap?  Keep in mind that with shipping and a 1-2 day turnaround time you will be without a laptop for 3-5 days.</li>
    </ol>

    <p>Below is the equipment check list of everything that need to be returned to the leasing company.  If any of the following equipment are not account for,
    see that attached email for instruction on how to file a Loss Asset Form to HR.  Contact your lease admin if you need further assistance with the process.</p>

    <ul style="list-style-type:disc">
      <li><strong><var>returnItems[0]</var></strong></li>
    </ul>

    <p>Please provide your mailing address and phone number. Return the old equipment via priority shipment to the following address. Always remember to provide the tracking information since it is the only way we can confirm the return of the equipment and also file a claim if the equipment is damaged during shipping.</p>

    <p>Thank you,<br>
      Your Yamaha Desktop Support Team</p>

    <p>CCTech<br>
    6600 Orangethorpe Avenue<br>
    Buena Park, CA 90620<br>
    714-522-9925</p>

    </body>
    </html>
    """
    msg = MIMEText(html, 'html')

    cr = "xxxxx"
    msg['Subject'] = 'CR:' + cr + " - Computer Swap"
    msg['From'] = cctech
    msg['To'] = recipiant

    part1 = MIMEText(html, 'html')

    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.login('ybp116.cctech','cH@rger16!')
    s.sendmail(cctech, recipiant, msg.as_string())
    s.quit()

    def desktop_lease_swap():


        html = """\
        <html>
        <head></head>
        <body>
        <p>Hi <var>fistName</var> <var>lastName</var>,</p>

        <p>Your new Dell Desktop (to replace <strong>TAGA<var>tagNumber</var></strong>) has arrived and we are beginning to configure it for deployment.<br>
        Could you please let us know the following information?</p>

        <ol type="1">
          <li>What is your network password? (You can always change it afterward, or call us directly to provide the password)</li>

          <li>What programs do you need us to ensure are installed and running properly?</li>

          <li>Use the attached document to schedule an appointment from the date of 8/9 to 9/1/2017 for your lease swap on SharePoint. If your target date is taken, contact us directly to see if we can accommodate your request. Keep in mind that we would need your current desktop for at least two hours (or more if you require Windows) to transfer your personal data and settings over to your new Mac.</li>
        </ol>

        <p>Below is the equipment check list of everything that need to be returned to the leasing company.  If any of the following equipment are not account for,
        see that attached email for instruction on how to file a Loss Asset Form to HR.  Contact your lease admin if you need further assistance with the process.</p>

        <ul style="list-style-type:disc">
          <li><strong><var>returnItems[0]</var></strong></li>
        </ul>

        <p>Thank you,<br>
          Your Yamaha Desktop Support Team</p>

        </body>
        </html>
        """
        msg = MIMEText(html, 'html')

        cr = "xxxxx"
        msg['Subject'] = 'CR:' + cr + " - Computer Swap"
        msg['From'] = cctech
        msg['To'] = recipiant

        part1 = MIMEText(html, 'html')

        s = smtplib.SMTP('smtp.gmail.com:587')
        s.ehlo()
        s.starttls()
        s.login('ybp116.cctech','cH@rger16!')
        s.sendmail(cctech, recipiant, msg.as_string())
        s.quit()

if __name__ == '__main__':
    mac_remote_lease_swap()
